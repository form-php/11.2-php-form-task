<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <title>Form Handling</title>
</head>
<body>
    <div class="container">
        <h2>Form Handling with PHP</h2>
        <form method="POST" action="">
            <div class="form-group">
                <label for="name">Name:</label>
                <input type="text" id="name" name="name">
            </div>
            <div class="form-group">
                <label for="password">Password:</label>
                <input type="password" id="password" name="password">
            </div>
            <div class="form-group-inline">
                <label>Gender:</label>
                <div class="radio-group">
                    <label for="male"><input type="radio" id="male" name="gender" value="male">Male</label>
                    <label for="female"><input type="radio" id="female" name="gender" value="female">Female</label>
                </div>
            </div>
            <div class="form-group-inline">
                <label>Hobbies:</label>
                <div class="checkbox-group">
                    <label for="game"><input type="checkbox" id="game" name="hobbies[]" value="Playing Game">Game</label>
                    <label for="reading"><input type="checkbox" id="reading" name="hobbies[]" value="Reading">Reading</label>
                    <label for="film"><input type="checkbox" id="film" name="hobbies[]" value="Watching Film">Film</label>
                </div>
            </div>
            <div class="form-group">
                <label for="comments">Comments:</label>
                <textarea id="comments" name="comments" cols="30" rows="10"></textarea>
            </div>
            <div class="form-group">
                <input type="submit" class="submit-btn" name="submit" value="Submit">
            </div>
        </form>

        <?php
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                $name = htmlspecialchars($_POST["name"]);
                $password = htmlspecialchars($_POST["password"]);
                $gender = isset($_POST["gender"]) ? htmlspecialchars($_POST["gender"]) : '';
                $hobbies = isset($_POST["hobbies"]) ? $_POST["hobbies"] : [];
                $comments = htmlspecialchars($_POST['comments']);

                $errors = [];

                if (empty($name)) {
                    $errors[] = "Name is required";
                }
                if (empty($password)) {
                    $errors[] = "Password is required";
                }
                if (empty($gender)) {
                    $errors[] = "Gender is required";
                }
                if (empty($hobbies)) {
                    $errors[] = "At least one hobby is required";
                }
                if (empty($comments)) {
                    $errors[] = "Comments are required";
                }

                if (count($errors) > 0) {
                    foreach ($errors as $error) {
                        echo "<p class='error'>$error</p>";
                    }
                } else {
                    echo "<p class='success'>Name: $name</p>";
                    echo "<p class='success'>Password: $password</p>";
                    echo "<p class='success'>Gender: $gender</p>";
                    echo "<p class='success'>Hobbies: " . implode(", ", $hobbies) . "</p>";
                    echo "<p class='success'>Comments: $comments</p>";
                }
            }
        ?>
    </div>
</body>
</html>